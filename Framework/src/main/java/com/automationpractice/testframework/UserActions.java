package com.automationpractice.testframework;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserActions {
	final WebDriver driver;

	public UserActions() {
		this.driver = Utils.getWebDriver();
	}

	public static void loadBrowser() {
		Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url"));
	}

	public static void quitDriver() {
		Utils.tearDownWebDriver();
	}

	//############# CLICK #########

	public void clickElement(String key) {
		Utils.LOG.info("Clicking on element " + key);
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
		element.click();
	}

	//############# TYPE #########

	public void typeValueInField(String value, String field) {
		Utils.LOG.info("Typing " + value + " in " + field);
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
		element.click();
		element.sendKeys(value);
	}
	public void selectFromDropdown(String id, String text){
		Utils.LOG.info("Selecting: " + text + "from dropdown menu ");
		Select dropdown = new Select(driver.findElement(By.id(id)));
		dropdown.selectByVisibleText(text);
	}

	//############# WAITS #########

	public void waitForElementVisible(String locator) {
		Utils.LOG.info("Waiting for element " + locator);
		WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	public void waitForElementClickable(String locator){
		Utils.LOG.info("Waiting for element " + locator);
		WebDriverWait wait = new WebDriverWait(driver, 20);
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Utils.getUIMappingByKey(locator))));
		element.click();
	}

	//############# ASSERTS #########

	public void assertElementPresent(String locator) {
		Utils.LOG.info("Assert element is present " + locator);
		waitForElementVisible(locator);
		Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
	}
	public void assertElementNotPresent(String locator) {
		Utils.LOG.info("Assert element " + locator + " is not present ");
		Assert.assertEquals(0, driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).size());
	}

	public String generateRandomEmail(String domain, int length) {
		return RandomStringUtils.random(length, "abcdefghijklmnopqrstuvwxyz") + "@" + domain;
	}

}
