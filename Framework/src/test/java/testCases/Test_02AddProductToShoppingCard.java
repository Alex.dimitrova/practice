package testCases;

import org.junit.Test;


public class Test_02AddProductToShoppingCard extends BaseTest {
    @Test
    public void addProduct(){
        actions.clickElement("category");
        actions.waitForElementVisible("shirt");
        actions.clickElement("shirt");
        actions.waitForElementVisible("addToCart");
        actions.clickElement("addToCart");
        actions.assertElementPresent("productIsAdded");
        //checkout
        actions.waitForElementClickable("checkoutButton");
        actions.assertElementPresent("total");
        actions.waitForElementClickable("continue");
        actions.waitForElementVisible("addressConfirmation");
        actions.waitForElementClickable("confirmAddress");
        actions.waitForElementVisible("termsAgreement");
        actions.clickElement("termsAgreement");
        actions.clickElement("confirmDelivery");
        actions.waitForElementVisible("paymentMethod");
        actions.clickElement("paymentMethod");
        actions.waitForElementVisible("confirmOrder");
        actions.clickElement("confirmOrder");
        actions.assertElementPresent("orderIsConfirmed");
    }
}
