package testCases;

import com.automationpractice.testframework.Utils;
import org.junit.Test;

public class Test_01CreateUser extends BaseTest{
    String email = actions.generateRandomEmail("ask.com", 8);
    @Test
    public void register(){
        actions.waitForElementVisible("homePageLogo");
        actions.clickElement("signIn");
        actions.waitForElementVisible("emailField");
        actions.typeValueInField(email, "emailField");
        actions.clickElement("createAccount");
        actions.assertElementPresent("userDetails");
        actions.waitForElementClickable("firstNameField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("firstName"), "firstNameField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("lastName"), "lastNameField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("password"), "passwordField");
        actions.waitForElementClickable("addressField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("address"), "addressField");
        actions.typeValueInField(Utils.getConfigPropertyByKey("city"), "cityField");
        actions.selectFromDropdown("id_state", "Alabama");
        actions.typeValueInField(Utils.getConfigPropertyByKey("postCode"), "postCode");
        actions.selectFromDropdown("id_country", "United States");
        actions.typeValueInField(Utils.getConfigPropertyByKey("mobilePhone"), "mobilePhoneField");
        actions.clickElement("register");
        actions.assertElementPresent("myAccountPage");

    }
}
