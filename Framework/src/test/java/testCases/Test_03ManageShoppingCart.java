package testCases;

import org.junit.Test;

public class Test_03ManageShoppingCart extends BaseTest{
    @Test
    public void manageCart(){
        actions.clickElement("category");
        actions.waitForElementVisible("dress");
        actions.clickElement("dress");
        actions.waitForElementVisible("addToCart");
        actions.clickElement("addToCart");
        actions.assertElementPresent("productIsAdded");
        actions.waitForElementClickable("checkoutButton");
        actions.assertElementPresent("total");
        actions.clickElement("removeFromCart");
        actions.assertElementNotPresent("dress");
    }
}
